<?php
    require('animal.php');
    require('frog.php');
    require('ape.php');

    // release 0
    $sheep = new Animal("shaun");

    echo "<br>Name : " . $sheep->name . "<br>";
    echo "Legs : " . $sheep->legs . "<br>";
    echo "Cold blooded : " . $sheep->cold_blooded . "<br>";

    // release 1 (frog)
    $kodok = new Frog("buduk");

    echo "<br>Name : " . $kodok->name . "<br>";
    echo "Legs : " . $kodok->legs . "<br>";
    echo "Cold blooded : " . $kodok->cold_blooded . "<br>";
    echo "Jump : ";
    $kodok->jump();

    // release 1 (Ape)
    $sungokong = new Ape("kera sakti");

    echo "<br><br>Name : " . $sungokong->name . "<br>";
    echo "Legs : " . $sungokong->legs . "<br>";
    echo "Cold blooded : " . $sungokong->cold_blooded . "<br>";
    echo "Yell : ";
    $sungokong->yell(); 


?>