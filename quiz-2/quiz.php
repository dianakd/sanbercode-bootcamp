
<?php

function ubah_huruf($string){
       
    $final = "";
    for ($x = 0; $x < strlen($string); $x++) {
        $temp = $string[$x];
        $final = ++$temp;
        echo $final;
      }
      echo "<br>";
    }
  // TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
echo "<br>";

function tukar_besar_kecil($string){
    
    $final="";
    $n = strlen($string);
    for($i=0;$i< $n;$i++){
        
       if(ctype_lower($string[$i])){
        $final.=strtoupper($string[$i]);
       }else{
        $final.=strtolower($string[$i]); 
       }
    }
    return $final . "<br>";
}

echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
?>