<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\cast;

class CastController extends Controller
{
    public function create(){
        return view('CRUD.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' =>  'required'
        ],
        [
            'nama.required' => 'nama harus terisi ya bund',
            'umur.required' => 'ettt umur juga jangan lupa',
            'bio.required'=> 'bio apalagi, isi yuk bund',
        ]
        );

        
        DB::table('cast')->insert ([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]); 

        return redirect('/cast')->with('success', 'Post Berhasil Tersimpan');
    }

    public function index(){
        $cast = DB::table('cast')->get();
        return view('CRUD.index', compact('cast'));
    }

    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('CRUD.show', compact('cast'));
    }

    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('CRUD.edit', compact('cast'));
    }

    public function update($id, Request $request){
        
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' =>  'required'
        ],
        [
            'nama.required' => 'nama harus terisi ya bund',
            'umur.required' => 'ettt umur juga jangan lupa',
            'bio.required'=> 'bio apalagi, isi yuk bund',
        ]
        );

        
        DB::table('cast')
         ->where('id', $id)
         ->update([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]); 

        return redirect('/cast');
    }

    public function destroy($id){
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }

}
