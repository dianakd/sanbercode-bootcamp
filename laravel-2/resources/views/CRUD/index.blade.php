@extends('adminlte.master')

@section('judulutama')
<h1>List Data Pemain</h1>
@endsection

@section('judul')
<h3 class="card-title">List Data</h3>
@endsection

@section('content')
<a type="button" class="btn btn-success" href="/cast/create">Tambahkan Data</a><br><br>
<div class="card">
    <div class="card-header">
      <h3 class="card-title">List Data Pemain</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0">
        
      <table class="table table-striped">
        <thead>
          <tr>
            <th>id</th>
            <th>Nama</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($cast as $key => $item)
            <tr>
              <td> {{ $key + 1 }}</td>
              <td> {{ $item->nama }} </td>

              <td>
                <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$item->id}}" type="button" class="btn btn-outline-primary btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" type="button" class="btn btn-outline-success btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
              </td>
            </tr>  
          @empty
            <tr>
              <td>Tidak ada di tabel cast </td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
@endsection
