@extends('adminlte.master')

@section('judul')
<h3 class="card-title">Read Page</h3>
@endsection

@section('content')
<a type="button" class="btn btn-success" href="/cast">kembali ke Data</a><br><br>
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Table Detail</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0">
        
      <table class="table table-striped">
        <thead>
          <tr>
            <th>id</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td> {{ $cast->id }} </td>
            <td> {{ $cast->nama }} </td>
            <td> {{ $cast->umur }} </td>
            <td> {{ $cast->bio }} </td>
          </tr>  
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection
