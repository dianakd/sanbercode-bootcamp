@extends('adminlte.master')

@section('judulutama')
<h1>Tambahkan Data Pemain</h1>
@endsection

@section('judul')
<h3 class="card-title">Tambahkan Data</h3>
@endsection

@section('content')
<a type="button" class="btn btn-primary" href="/cast">Kembali Ke MainPage</a><br><br>
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Create New Cast</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/cast" method="POST">
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Nama</label>
          <input type="text" class="form-control" name="nama" id="nama" value="{{ old('nama', '') }}" placeholder="Masukkan Nama">
          @error('nama')
          <br><div class="alert alert-danger alert-dismissible">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Umur</label>
          <input type="number" class="form-control" name="umur" id="umur" value="{{ old('umur', '') }}" placeholder="Masukkan Umur">
          @error('umur')
          <br><div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Biodata</label>
            <input type="text" class="form-control" name="bio" id="bio" value="{{ old('bio', '') }}" placeholder="Biodata">
            @error('bio')
            <br><div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
            @enderror
          </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
@endsection
