@extends('adminlte.master')

@section('judulutama')
<h1>Edit Data Pemain</h1>
@endsection

@section('judul')
<h3 class="card-title">Edit Data</h3>
@endsection

@section('content')
<a type="button" class="btn btn-success" href="/cast">kembali ke Data</a><br><br>
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edit Cast ke - {{$cast->id}}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
      <div class="card-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Nama</label>
          <input type="text" class="form-control" name="nama" id="nama" value="{{ $cast->nama }}">
          @error('nama')
          <br><div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Umur</label>
          <input type="number" class="form-control" name="umur" id="umur" value="{{ $cast->umur }}" >
          @error('umur')
          <br><div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Biodata</label>
            <input type="text" class="form-control" name="bio" id="bio" value="{{ $cast->bio }}" >
            @error('bio')
            <br><div class="alert alert-danger alert-dismissible fade show">{{ $message }}</div>
            @enderror
          </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Edit</button>
      </div>
    </form>
  </div>
@endsection
