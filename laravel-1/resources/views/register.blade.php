<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Bootcamp SanberCode</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    </head>
        <body>
    
            <nav class="navbar navbar-expand-xxl navbar-light bg-light">
                <b class="navbar-brand " style="margin-left: 10px;" href="#">SanberBook</b>
            </nav>
            
            <div class="container-fluid">
                <div class="mx-5" >
                    <div class="container-fluid mt-4">
                        <h2 style="font-weight: bold;">Buat Account Baru!</h2>
                        <p style="font-weight: bold;">Sign Up Form</p>
                    </div>
                    <form action = "/welcome" method="POST">
                        @csrf
                        <div class="row mx-1">
                            <div class="row mb-3">
                            <div class="col">
                                <div class="form-outline">
                                    <label class="form-label" for="namadepan">First name:</label>
                                        <input name="namadepan" type="text" id="namadepan" class="form-control" />
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-outline">
                                    <label class="form-label" for="namabelakang">Last name:</label>
                                        <input name="namabelakang" type="text" id="namabelakang" class="form-control" />
                                </div>
                            </div>
                            </div>
                            
                            <div id="box" class="col mb-2">
                                <label class="form-label" for="gender">Gender: </label>
                                  <div class="form-check mb-2">
                                    <input class="form-check-input" type="radio" name="flexRadioDefault" id="cowo"checked>
                                    <label class="form-check-label" for="flexRadioDefault1">
                                      Male
                                    </label>
                                  </div>
                                  <div class="form-check mb-2">
                                    <input class="form-check-input" type="radio" name="flexRadioDefault" id="cewe">
                                    <label class="form-check-label" for="flexRadioDefault2">
                                      Female
                                    </label>
                                  </div>
                                  <div class="form-check mb-2">
                                    <input class="form-check-input" type="radio" name="flexRadioDefault" id="other">
                                    <label class="form-check-label" for="flexRadioDefault3">
                                      Others
                                    </label>
                                  </div>
                                </div>
                            </div>

                            <div class="row mx-1">
                                <div class="col form-outline mb-4">
                                        <label for="formselect" class="form-label">Nationality:</label>
                                        <select class="form-select" id="formselect" required>
                                          <option>Indonesian</option>
                                          <option>Singapore</option>
                                          <option>Malaysian</option>
                                          <option>Korean</option>
                                        </select>
                                </div>
                            </div>
                        
                            <div class="row mx-1">
                                <div class="col mb-2">
                                        <label class="form-label" for="language">Language Spoken: </label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault"checked/>
                                            <label class="form-check-label" for="bindo">Bahasa Indonesia</label>
                                        </div>
                                        
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked"/>
                                            <label class="form-check-label" for="bing">English</label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault"/>
                                            <label class="form-check-label" for="bother">Other</label>
                                        </div>
                                </div>
                            </div>
                            
                            <div class="row mx-1">
                                <div class="form-outline mb-4">
                                    <label class="form-label" for="bio">Bio:</label>
                                    <textarea class="form-control" id="bio" rows="4"></textarea>
                                </div>
                            </div>
                        
                            <div class="form-check d-flex justify-content-center mb-4">
                                <input class="form-check-input me-2" type="checkbox" value="" id="centangakhir" checked />
                                <label class="form-check-label" for="centangakhir"> Create an account? </label>
                            </div>
                        
                            <div class="text-center">
                                <input type="submit" class="btn btn-primary btn-block mb-4 text-center">
                            </div>
                            
                        </div>
                      </form>
                </div>
            </div>
            
        </body>
</html>