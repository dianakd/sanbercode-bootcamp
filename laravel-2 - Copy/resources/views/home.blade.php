<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Bootcamp SanberCode</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    </head>
        <body>
    
            <nav class="navbar navbar-expand-xxl navbar-light bg-light">
                <b class="navbar-brand " style="margin-left: 10px;" href="#">SanberBook</b>
            </nav>

            <div class="container-fluid mt-4">
                <h2>Social Media Developer Santai Berkualitas</h2>
                <p>Belajar dan Berbagi agar hidup ini semakin santai dan berkualitas</p>
            </div>
            
            <div class="container-fluid mt-4">
                <h2>Benefit Join di SanberBook</h2>
                    <div class="ms-4">
                        <li>Mendapatkan motivasi dari sesama Developer</li>
                        <li>Sharing knowledge dari para mastah Sanber</li>
                        <li>Dibuat Oleh calon web developer terbaik</li>
                    </div>
            </div>
            
            <div class="container-fluid mt-4">
                <h2>Cara Bergabung ke SanberBook</h2>
                    <div class="ms-2">
                        <ol>
                            <li>Mengunjungi Website ini</li>
                            <li>
                                Mendaftar di
                                <a href="/register"> Form Sign Up</a>
                            </li>
                            <li>Selesa!</li>
                        </ol>
                    </div>
            </div>

        
        </body>
</html>