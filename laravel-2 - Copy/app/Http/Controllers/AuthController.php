<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return View('register');
    }

    public function welcome (Request $request){
        $namadepan = $request->namadepan;
        $namabelakang = $request->namabelakang;

        return view('welcome', compact ('namadepan', 'namabelakang'));
    }
}
